# DNSSEC Bootstrapping

Maintain a local Signalling Zone per [Automatic DNSSEC Bootstrapping using Authenticated Signals from the Zone's Operator](https://datatracker.ietf.org/doc/draft-ietf-dnsop-dnssec-bootstrapping/).

Using a catalog zone transferred from localhost as the dynamic source
for the list of Child zones, check the apex of each Child for CDS and
CDNSKEY RRsets, and replicate them (or their absence) via UPDATE
messages to the corresponding Signalling Name within a Signalling Zone
for which this server is the primary.


## Usage

```
dnssec-bootstrap-cron.py [configfile.yml]
```


## Configuration

The configuration file must set `catalog_zone` and `signal_zone`. The
default location for the configuration file is:

```
/usr/local/etc/dnssec-bootstrap.yml
```

## Authentication

All zone transfers, queries, and updates are performed against localhost
without further authentication.
