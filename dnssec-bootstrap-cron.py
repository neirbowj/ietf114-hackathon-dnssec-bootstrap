#!/usr/bin/env python

"""
Maintain a local Signalling Zone per

    Automatic DNSSEC Bootstrapping using Authenticated Signals from
    the Zone's Operator [0]

Using a catalog zone transferred from localhost as the dynamic source
for the list of Child zones, check the apex of each Child for CDS and
CDNSKEY RRsets, and replicate them (or their absence) via UPDATE
messages to the corresponding Signalling Name within a Signalling Zone
for which this server is the primary.

Usage: dnssec-bootstrap-cron.py [configfile.yml]

The configuration file must set `catalog_zone` and `signal_zone`. The
default location for the configuration file is:

    /usr/local/etc/dnssec-bootstrap.yml

All zone transfers, queries, and updates are performed against localhost
without further authentication.

[0] https://datatracker.ietf.org/doc/draft-ietf-dnsop-dnssec-bootstrapping/
"""

import sys

import dns.name
import dns.query
import dns.rdataclass
import dns.rdatatype
import dns.update
import yaml


DEFAULT_CONFFILE = "/usr/local/etc/dnssec-bootstrap.yml"
LOCALHOST_IP = '127.0.0.1'


class ConfigurationInvalidError(ValueError):
    pass


def load_config_from_file(f):
    config = yaml.load(f, Loader=yaml.Loader)

    if 'signal_zone' not in config:
        raise ConfigurationInvalidError("missing `signal_zone`")

    if 'catalog_zone' not in config:
        raise ConfigurationInvalidError("missing `catalog_zone`")

    return config


def load_config(location=DEFAULT_CONFFILE):
    with open(location) as f:
        return load_config_from_file(f)


def zones_from_catalog(catalog_zone_name, server=LOCALHOST_IP):
    """
    Transfer `catalog_zone_name` from `server`, yield the target
    name (zone) from each PTR found. Raise `ValueError` if a PTR
    RRset has more than one RR (i.e. non-unique catalog label).
    """
    for message in dns.query.xfr(server, catalog_zone_name):
        for rrset in message.answer:
            if rrset.match(dns.rdataclass.IN,
                           dns.rdatatype.PTR,
                           dns.rdatatype.NONE):

                if len(rrset.items) > 1:
                    raise ValueError("Non-unique catalog label:\n"
                                     f"{rrset}")
                zone = rrset[0].target
                yield zone


def rrsets_index_by_nametype(rrsets):
    idx = {}
    for rrset in rrsets:
        if (rrset.name, rrset.rdtype) in idx:
            rdtype_name = dns.rdatatype.to_text(rrset.rdtype)
            raise ValueError("Found unexpected duplicate in "
                             "answer section: "
                             f"{rrset.name}/{rdtype_name}")
        idx[rrset.name, rrset.rdtype] = rrset
    return idx


def section_diff(a, b):
    """
    `a` and `b` are lists of RRset objects. Starting with `a`,
    remove each RR that also appears in `b` as well as any
    RRSIGs. Return the result.
    """
    a_idx = rrsets_index_by_nametype(a)
    b_idx = rrsets_index_by_nametype(b)

    diff = []
    for nametype, rrset in a_idx.items():
        if nametype[1] == dns.rdatatype.RRSIG:
            continue
        if nametype not in b_idx:
            diff.append(rrset)
        else:
            diff.append(rrset.difference(b_idx[nametype]))

    return diff


def replicate_rrset(
        rdtype,
        child_zone,
        child_zone_server,
        signal_zone,
        signal_zone_primary,
        signal_type=None):
    """
    Look up the RRset of type `rdtype` from the apex of `child_zone`
    directly via `child_zone_server`, and replicate it (or its absence)
    to `signal_zone` via `signal_zone_primary` after applying the
    appropriate name transformation to a signalling name of
    `signal_type` (default: "_dsboot").
    """

    if signal_type is None:
        signal_type = dns.name.from_text('_dsboot')

    query = dns.message.make_query(child_zone, rdtype, flags=0)
    child_response = dns.query.udp(query, child_zone_server)
    rcode = dns.rcode.from_flags(child_response.flags,
                                 child_response.ednsflags)
    if rcode not in (dns.rcode.NOERROR, dns.rcode.NXDOMAIN):
        rcode_name = dns.rcode.to_text(rcode)
        rdtype_name = dns.rdatatype.to_text(rdtype)
        print(f"WARNING: status {rcode_name} for "
              f"{child_zone}/{rdtype_name}")
        return

    signalling_name = (
            signal_type.relativize(dns.name.root)
            + child_zone.relativize(dns.name.root)
            + signal_zone
            )

    query = dns.message.make_query(signalling_name, rdtype, flags=0)
    signal_response = dns.query.udp(query, signal_zone_primary)
    rcode = dns.rcode.from_flags(signal_response.flags,
                                 signal_response.ednsflags)
    if rcode not in (dns.rcode.NOERROR, dns.rcode.NXDOMAIN):
        rcode_name = dns.rcode.to_text(rcode)
        rdtype_name = dns.rdatatype.to_text(rdtype)
        print(f"WARNING: status {rcode_name} for "
              f"{signalling_name}/{rdtype_name}")
        return

    to_add = section_diff(child_response.answer, signal_response.answer)
    to_del = section_diff(signal_response.answer, child_response.answer)

    if len(to_add) == 0 and len(to_del) == 0:
        # nothing to do
        return

    update = dns.update.UpdateMessage(signal_zone)
    for rrset in to_del:
        update.delete(signalling_name, rrset)
    for rrset in to_add:
        update.add(signalling_name, rrset)

    response = dns.query.udp(update, signal_zone_primary)
    rcode = dns.rcode.from_flags(response.flags, response.ednsflags)
    if rcode != dns.rcode.NOERROR:
        print(f"ERROR:\n{update}\n{response}")


def main():
    try:
        conffile = sys.argv[1]
    except IndexError:
        conffile = DEFAULT_CONFFILE

    config = load_config(conffile)

    signal_zone_name = dns.name.from_text(config['signal_zone'])

    catalog_zone_name = dns.name.from_text(config['catalog_zone'])
    for child_zone in zones_from_catalog(catalog_zone_name):
        for rdtype in (dns.rdatatype.CDS, dns.rdatatype.CDNSKEY):
            replicate_rrset(
                    rdtype,
                    child_zone,
                    LOCALHOST_IP,
                    signal_zone_name,
                    LOCALHOST_IP
            )


if __name__ == '__main__':
    main()
